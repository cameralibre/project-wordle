import React from "react";
import { NUM_OF_GUESSES_ALLOWED } from "../../constants";
import { range } from "../../utils";
import Guess from "../Guess";

function GuessResults({ guesses, answer }) {
  return (
    <ul className="guess-results">
      {range(NUM_OF_GUESSES_ALLOWED).map((num) => {
        const guess = guesses[num];

        return (
          <li
            key={num}
            aria-hidden={!guess}
            aria-label={guess}
            className="guess"
          >
            <Guess isEmpty={!guess} value={guess} answer={answer} />
          </li>
        );
      })}
    </ul>
  );
}

export default GuessResults;
