import React from "react";
import Banner from "../Banner/Banner";

function YouWin({ numOfGuesses }) {
  return (
    <Banner won={true}>
      <p>
        <strong>Congratulations!</strong> Got it in{" "}
        <strong>
          {numOfGuesses === 1 ? "1 guess" : `${numOfGuesses} guesses`}
        </strong>
        .
      </p>
    </Banner>
  );
}

export default YouWin;
