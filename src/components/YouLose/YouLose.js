import React from "react";
import Banner from "../Banner";

function YouLose({ answer }) {
  return (
    <Banner won={false}>
      <p>
        Sorry, the correct answer is <strong>{answer}</strong>.
      </p>
    </Banner>
  );
}

export default YouLose;
