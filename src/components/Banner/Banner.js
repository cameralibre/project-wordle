import React from "react";

function Banner({ won, children }) {
  return <div className={won ? "happy banner" : "sad banner"}>{children}</div>;
}
export default Banner;
