import React from "react";
import { checkGuess } from "../../game-helpers";
import { range } from "../../utils";
import { Fragment } from "react/cjs/react.production.min";

function Guess({ isEmpty, value, answer }) {
  if (isEmpty) {
    return range(0, 5).map((n) => <span key={n} className="cell"></span>);
  }

  const result = checkGuess(value, answer);

  return result.map(({ letter, status }, index) => (
    <Fragment key={index}>
      <span className={`cell ${status}`}>{letter}</span>
      <span className="visually-hidden">
        {letter}: {status}
      </span>
    </Fragment>
  ));
}

export default Guess;
