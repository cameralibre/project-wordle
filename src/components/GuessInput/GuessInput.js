import React, { useState } from "react";
import { VALID_GUESSES } from "../../data";

function GuessInput({ prevGuesses, submitGuess, gameOver }) {
  const [guess, setGuess] = useState("");
  const [isValidLengthError, setIsValidLengthError] = useState(false);
  const [isValidWordError, setIsValidWordError] = useState(false);
  const [alreadySubmitted, setAlreadySubmitted] = useState("");

  function handleSubmit(event) {
    // debugger;
    event.preventDefault();
    if (guess.length < 5) {
      setIsValidLengthError(true);
      return;
    }
    setIsValidLengthError(false);

    if (prevGuesses.includes(guess)) {
      setAlreadySubmitted(guess);
      return;
    }
    setAlreadySubmitted("");

    if (!VALID_GUESSES.includes(guess)) {
      setIsValidWordError(true);
      return;
    }
    setIsValidWordError(false);

    submitGuess(guess);
    setGuess("");
  }

  return (
    <form
      className={
        gameOver ? "guess-input-wrapper fade-out" : "guess-input-wrapper"
      }
      onSubmit={handleSubmit}
    >
      <label htmlFor="guess-input">Enter guess:</label>
      <input
        id="guess-input"
        type="text"
        required
        disabled={gameOver}
        maxLength={5}
        value={guess}
        onChange={(event) => {
          const sanitizedInput = event.target.value
            .toUpperCase()
            .replace(/[^A-Z]+/g, "");
          setGuess(sanitizedInput);
        }}
        aria-errormessage="guess-error"
        aria-invalid={
          isValidLengthError || isValidWordError || alreadySubmitted.length
        }
      />
      {isValidLengthError || isValidWordError || alreadySubmitted !== "" ? (
        <p id="guess-error" className="guess-input__error-message">
          Error:{" "}
          {isValidLengthError
            ? "Your guess must be 5 letters long"
            : alreadySubmitted.length
            ? `${alreadySubmitted} has already been submitted. Try again.`
            : "Your guess must be a valid word"}
        </p>
      ) : (
        <p id="guess-hint" className="guess-input__hint">
          Your guess must be a 5-letter word
        </p>
      )}
    </form>
  );
}

export default GuessInput;
