import React, { useState, StrictMode } from "react";

import { sample } from "../../utils";
import { WORDS } from "../../data";
import { NUM_OF_GUESSES_ALLOWED } from "../../constants";
import GuessInput from "../GuessInput";
import GuessResults from "../GuessResults/GuessResults";
import YouWin from "../YouWin";
import YouLose from "../YouLose";

// Pick a random word on every pageload.
const answer = sample(WORDS);
// To make debugging easier, we'll log the solution in the console.
console.info({ answer });

function Game() {
  const [guesses, setGuesses] = useState([]);
  const handleSubmitGuess = (newGuess) => setGuesses([...guesses, newGuess]);

  const youWin = guesses.some((guess) => guess === answer);
  const youLose = !youWin && guesses.length === NUM_OF_GUESSES_ALLOWED;
  const gameOver = youWin || youLose;

  return (
    <>
      <StrictMode>
        <GuessResults guesses={guesses} answer={answer} />

        <GuessInput
          prevGuesses={guesses}
          submitGuess={handleSubmitGuess}
          gameOver={gameOver}
        />

        {youWin && <YouWin numOfGuesses={guesses.length} />}
        {youLose && <YouLose answer={answer} />}
      </StrictMode>
    </>
  );
}

export default Game;
